# Bullying In College Its Effects On Education


Bullying has been a prominent concern since the 18th century. However, back then, it was classified as a form of physical harassment related to isolation, death, and even worse. But we all know that bullying isn’t classified as mere bodily injury.
While most still believe bullying only happens in high school, it’s also been proven to persist later on in life. The Centers for Disease Control states that bullying is an aggressive and repeated behavior. Being bullied in college, on top of your coursework stress, makes your education suffer multitudes. Find the various ways it could affect you and your mental wellbeing below.
## Academic Performance
Bullying can come in multiple forms. You might be experiencing a roommate who keeps demeaning you or a friend who embarrasses you on purpose. No matter what the issue is, the consequences of [bullying](https://edubirdie.com/examples/bullying/) can lead to a massive dent in your academic performance. Students seem to find it challenging to concentrate under excessive pressure from others. Being targetted by someone means you’re preoccupied with other issues in your life, which results in less time dedicated to studies. Most bullied students end up with lower GPAs, telling their mental and academic states to suffer severely.
## Dropouts
More than half of the dropouts stated that their reason for doing so was their mental health. But a good chunk leaves university simply because of the bullying they were subjected to. Think about it; if someone doesn’t feel welcomed in an environment, why would they force the pain and struggle upon themselves?
## Drug Abuse
Once excessive bullying occurs, or even social or academic pressure, people tend to slowly resort to drugs. While that doesn’t happen to everyone, multiple people seek a sense of numbness or forgetfulness under stress. Hence, you’ll notice that drug abuse is more prevalent in students with mental illnesses or bullying issues.
Moreover, the warning signs that someone is being bullied and the signs of substance abuse are often very similar. They share multiple symptoms. Some include depression, anxiety, sleeping disorders, eating problems, and withdrawal from social activities.
## Suicide
Did you know that suicide is the [second most prevalent](https://caps.umich.edu/article/facts-and-statistics-0#:~:text=Suicide%20is%20the%20%232%20leading,of%20someone%20who%20has%20attempted) cause of death in college students? Bullying often contributes to a student’s suicidal thoughts. While bullying alone doesn’t cause a bullied kid to develop suicidal thoughts, it usually does in a college. Everything changes once you’re off to your dorm room and in a different environment. Hence, you won’t have the same family and friends support system around you. Being bullied in a foreign atmosphere often leads to isolation and depressive feelings.
## Various Types of Bullying
Keep in mind that bullying can come in various forms. It could be physical, emotional, or even verbal abuse. More recently, cyberbullying has increased. That generally means that the bully is hidden behind a computer screen. Hence, it’s much harder to identify who the person could be. Addressing the problem becomes much more challenging. 
## Conclusion
Bullying is a prevalent issue in our modern age; eliminating it is pretty challenging. Punishment should not be the go-to solution. The first step toward fixing the problem would be raising awareness about the persistent issues.

